# azure

[![Build Status](https://api.travis-ci.org/nortonlifelock/azure.svg?branch=master)](https://travis-ci.org/nortonlifelock/azure)
[![Go Report Card](https://goreportcard.com/badge/github.com/nortonlifelock/azure)](https://goreportcard.com/report/github.com/nortonlifelock/azure)
[![GoDoc](https://godoc.org/github.com/nortonlifelock/azure?status.svg)](https://godoc.org/github.com/nortonlifelock/azure)
[![License](https://img.shields.io/badge/License-Apache%202.0-blue.svg)](https://opensource.org/licenses/Apache-2.0) [![PRs Welcome](https://img.shields.io/badge/PRs-welcome-brightgreen.svg)](http://makeapullrequest.com)

azure
