module github.com/nortonlifelock/azure

go 1.13

require (
	github.com/Azure/azure-sdk-for-go v38.2.0+incompatible
	github.com/Azure/go-autorest/autorest v0.9.4
	github.com/Azure/go-autorest/autorest/azure/auth v0.4.2
	github.com/nortonlifelock/aegis v1.0.1-0.20200214181401-3a5d0fe8c62c // indirect
	github.com/nortonlifelock/aws v1.0.0
	github.com/nortonlifelock/domain v1.0.1-0.20200131202818-36fd209d3c7b
	github.com/nortonlifelock/log v1.0.1-0.20200129171320-c4a4dd839ed8
)
